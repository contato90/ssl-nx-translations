# StreamerSonglist Translations

## Current Languages

- cs - Czech
- de - German
- en - English
- es - Spanish
- fr - French
- ja - Japanese
- pt-BR - Portuguese (Brazil)
- ru - Russian
- zh-CN - Chinese (Simplified)

